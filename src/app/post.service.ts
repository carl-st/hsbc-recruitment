import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { PostModel } from './model/post.model';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})

export class PostService {
  private apiUrl = 'https://jsonplaceholder.typicode.com';
  error = '';

  constructor(private http: HttpClient) { }

  getPosts (): Observable<PostModel[]> {
    return this.http.get<PostModel[]>(`${this.apiUrl}/posts`);
  }

  getPostDetails (postId): Observable<PostModel> {
    return this.http.get<PostModel>(`${this.apiUrl}/posts/${postId}`);
  }
}
