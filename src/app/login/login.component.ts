import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.less']
})
export class LoginComponent implements OnInit {
  user: string;
  password: string;
  title = 'Login';

  constructor(private router: Router) { }

  ngOnInit() {

  }

  onLogin(user: string, password: string): void {
    sessionStorage.setItem('user', user);
    sessionStorage.setItem('password', password); // should not be keeped explicitly
    this.router.navigate(['']);
  }

}
