import { Component, OnInit, TemplateRef } from '@angular/core';
import { PostModel } from '../model/post.model';
import { PostService } from '../post.service';
import { ActivatedRoute } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.less']
})
export class DetailsComponent implements OnInit {
  bsModalRef: BsModalRef;
  post: PostModel = new PostModel;
  postId: number;
  error = '';

  constructor(
    private timelineService: PostService,
    private route: ActivatedRoute,
    private modalService: BsModalService
  ) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.openModal(error.message);
      this.error = `${operation} failed: ${error.message}`;
      return of(result as T);
    };
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.postId = params['id'];
    });
    this.getPostDetails();
  }

  openModal(message) {
    const initialState = {
      message
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
  }

  getPostDetails(): void {
    this.timelineService.getPostDetails(this.postId)
    .pipe(
      catchError(this.handleError<PostModel>(`getPostDetails id=${this.postId}`))
    ).subscribe(post => this.post = post);
  }

}
