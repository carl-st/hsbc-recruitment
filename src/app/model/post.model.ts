class PostModel {
    body: string;
    id: number;
    title: string;
    userId: number;
}

export {
    PostModel
};
