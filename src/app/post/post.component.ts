import { Component, OnInit, Input } from '@angular/core';
import { PostModel } from '../model/post.model';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.less']
})
export class PostComponent implements OnInit {
  @Input() title: string;
  @Input() userId: number;

  constructor(post: PostModel) {
    this.title = post.title;
    this.userId = post.userId;
  }

  ngOnInit() {
  }

}
