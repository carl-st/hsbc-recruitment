import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ModalModule, BsModalService } from 'ngx-bootstrap';
import { FilterPipe } from './filter.pipe';

import { AppComponent } from './app.component';
import { TimelineComponent } from './timeline/timeline.component';
import { PostComponent } from './post/post.component';
import { AppRoutingModule } from './app-routing.module';
import { DetailsComponent } from './details/details.component';
import { LoginComponent } from './login/login.component';
import { AuthGuardService as AuthGuard } from './auth-guard.service';
import { AuthService } from './auth.service';
import { ModalComponent } from './modal/modal.component';
import { NoContentComponent } from './no-content/no-content.component';

@NgModule({
  declarations: [
    AppComponent,
    TimelineComponent,
    PostComponent,
    DetailsComponent,
    LoginComponent,
    FilterPipe,
    ModalComponent,
    NoContentComponent
  ],
  entryComponents: [
    ModalComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    ModalModule.forRoot()
  ],
  providers: [
    AuthGuard,
    AuthService,
    BsModalService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
