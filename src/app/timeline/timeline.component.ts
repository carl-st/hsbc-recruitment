import { Component, OnInit } from '@angular/core';
import { PostModel } from '../model/post.model';
import { PostService } from '../post.service';
import { Router } from '@angular/router';
import { catchError, map, tap } from 'rxjs/operators';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Observable, of } from 'rxjs';
import { ModalComponent } from '../modal/modal.component';

@Component({
  selector: 'app-timeline',
  templateUrl: './timeline.component.html',
  styleUrls: ['./timeline.component.less'],
  providers: [PostModel]
})
export class TimelineComponent implements OnInit {
  bsModalRef: BsModalRef;
  posts = [];
  search = '';
  error = '';

  constructor(
    private timelineService: PostService,
    private router: Router,
    private modalService: BsModalService
  ) { }

  ngOnInit() {
    this.getPosts();
  }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      this.openModal(error.message);
      this.error = `${operation} failed: ${error.message}`;
      return of(result as T);
    };
  }

  openModal(message) {
    const initialState = {
      message
    };
    this.bsModalRef = this.modalService.show(ModalComponent, { initialState });
  }

  getPosts(): void {
    this.timelineService.getPosts()
    .pipe(
      catchError(this.handleError<PostModel[]>(`getPosts`))
    ).subscribe(posts => {
        if (posts) {
          posts.sort((a, b) => b.id - a.id);
        }
        this.posts = posts;
      });

  }

  onLogout() {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }

}
